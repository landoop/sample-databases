# Northwind e-commerce sample database

Source: https://code.google.com/archive/p/northwindextended/

License: [Artistic License/GPL](http://dev.perl.org/licenses/)

# Information

The Northwind database is an excellent tutorial schema for a small-business ERP,
with customers, orders, inventory, purchasing, suppliers, shipping, employees,
and single-entry accounting.

# Other

You may find another version of Northwind's database, yet with a _non-commercial use_
license at [github.com/dalers/mywind](https://github.com/dalers/mywind).
