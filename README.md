# Landoop Sample Databases #

Various databases (schema and data) that we offer to our users to experiment on.
Some may be ours, some we gathered from the web. Please check each database directory for information on source and license.
